import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { TodoComponent } from './todo/todo.component';
import { HttpClientModule } from '@angular/common/http';

import { HttpErrorHandler } from './handlers/http-error-handler.service';
import { MessageService } from './services/message.service';

@NgModule({
  declarations: [
    TodoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    MessageService,
    HttpErrorHandler
  ],
  bootstrap: [TodoComponent]
})
export class AppModule { }
