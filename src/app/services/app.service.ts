import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../handlers/http-error-handler.service';

export interface BaseTodoReturn {
  content: ContentData;
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

export interface ContentData {
  count: number;
  rows: Todo[];
}

export interface Todo {
  id: number;
  message: string;
  created_at: Date;
}


@Injectable()
export class AppService {
  todoApi = 'http://localhost:3000/v1/todo';
  private handleError: HandleError;

  /**
   * That function will return a Promisse with a list of Todo on Content.
   * @param dateTime = it will define created_time of results must be $gt than dateTime
   */
  getTodos(dateTime: Date = null) {
    if (dateTime) {
      return this.http.get(`${this.todoApi}/?created_at=${dateTime}`);
    }
    return this.http.get(this.todoApi);
  }

  /**
   * It will return 201 if created or will return some validation errors.
   * @param todo = Todo item to be created
   */
  addTodo(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(this.todoApi, todo, httpOptions)
      .pipe(
        catchError(this.handleError('addTodo', todo))
      );
  }

  /**
   * It will return void if 200 on success or 404 if item not found.
   * @param id = id of todo item
   */
  deleteTodo(id: number): Observable<Todo> {
    return this.http.delete<void>(`${this.todoApi}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteTodo', null))
      );
  }

  constructor(private http: HttpClient,
              httpErrorHandler: HttpErrorHandler) {
      this.handleError = httpErrorHandler.createHandleError('AppService');
    }
}
