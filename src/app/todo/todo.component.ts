import { Component, OnInit } from '@angular/core';
import { AppService, BaseTodoReturn, Todo, ContentData } from '../services/app.service';

@Component({
  selector: 'app-root',
  providers: [ AppService ],
  templateUrl: './todo.component.pug',
  styleUrls: ['./todo.component.scss']
})

// TODO - Some improvements.
// We can split it in some components like todo items and some custom forms,
// we can remove inline styles too.
export class TodoComponent implements OnInit {

  error: any;

  contentData: ContentData = { rows : [], count : 0 };

  validationErrors = {};

  constructor(private appService: AppService) {}

  ngOnInit() { this.loadTodos(); }

  loadTodos() {
    this.appService.getTodos()
      .subscribe(
        (data: BaseTodoReturn) => {
          this.contentData = data.content;
        },
        error => this.error = error
      );
  }

  deleteTodo( id: number ) {
    this.appService.deleteTodo(id)
    .subscribe(
      () => {
        this.contentData.rows.find((d, idx) => {
          if (d.id === id) {
            // TODO - we can do it better overriding prototype functions to do itself
            this.contentData.rows.splice(idx, 1);
            this.contentData.count--;
            return true;
          } else {
            return false;
          }
        });
      },
      error => {
        this.validationErrors = error.error.errors.validationErrors;
      }
    );
  }

  // A search using page time and an offset works best when we delete an element.
  // For example: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] if we remove 5 previous items and do not load the 5 previous items,
  // to try to navigate to a next page. the return income [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
  // thus loading the same data twice.
  // With a date of use you can return the data related to a given. The opposite is a situation where
  // backend is not working uniquetimestamp.
  loadMore(): void {
    const lastTodo = this.contentData.rows[this.contentData.rows.length - 1];

    const lastDate = lastTodo ? lastTodo.created_at : null;

    this.appService.getTodos(lastDate)
    .subscribe(
      (data: BaseTodoReturn) => {
        data.content.rows.forEach(todo => this.contentData.rows.push(todo));
      },
      error => this.error = error
    );

  }

  addTodo(message: string): void {
    const todo: Todo = { message } as Todo;
    this.validationErrors = {};
    this.appService.addTodo(todo)
      .subscribe(
        newTodo => {
          this.contentData.rows.push(newTodo);
          this.contentData.count++;
        },
        error => {
          this.validationErrors = error.errors;
        }
      );
  }

}
